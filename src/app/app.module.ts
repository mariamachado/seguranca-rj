import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MulheresPage } from "../pages/mulheres/mulheres";
import { LgbtPage } from "../pages/lgbt/lgbt";
import { DiscriminacaoRacialPage } from "../pages/discriminacao-racial/discriminacao-racial";
import { IdososPage } from "../pages/idosos/idosos";
import { CriancaAdolescentePage } from "../pages/crianca-adolescente/crianca-adolescente";
import { IntoleranciaReligiosaPage } from "../pages/intolerancia-religiosa/intolerancia-religiosa";
import { InformacoesPage } from "../pages/lgbt/informacoes/informacoes";
import { DefinicoesPage } from "../pages/lgbt/informacoes/definicoes/definicoes";
import { DetalhesDefinicaoPage } from '../pages/lgbt/informacoes/definicoes/detalhes-definicao/detalhes-definicao';
import { EnderecosPage } from '../pages/lgbt/enderecos/enderecos';

import { UsuariosProvider } from '../providers/usuarios/usuarios';

// Recursos nativos adicionais
import { DocumentViewer } from '@ionic-native/document-viewer';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { CallNumber } from '@ionic-native/call-number';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MulheresPage,
    LgbtPage,
    DiscriminacaoRacialPage,
    IdososPage,
    CriancaAdolescentePage,
    IntoleranciaReligiosaPage,
    InformacoesPage,
    DefinicoesPage,
    DetalhesDefinicaoPage,
    EnderecosPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MulheresPage,
    LgbtPage,
    DiscriminacaoRacialPage,
    IdososPage,
    CriancaAdolescentePage,
    IntoleranciaReligiosaPage,
    InformacoesPage,
    DefinicoesPage,
    DetalhesDefinicaoPage,
    EnderecosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UsuariosProvider,
    DocumentViewer,
    LaunchNavigator,
    CallNumber
  ]
})
export class AppModule {}