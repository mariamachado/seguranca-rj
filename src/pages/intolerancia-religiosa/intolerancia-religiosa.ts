import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-intolerancia-religiosa',
  templateUrl: 'intolerancia-religiosa.html',
})
export class IntoleranciaReligiosaPage{

  constructor(public navCtrl: NavController, public navParams: NavParams){}

  ionViewDidLoad(){}

}
