import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntoleranciaReligiosaPage } from './intolerancia-religiosa';

@NgModule({
  declarations: [
    IntoleranciaReligiosaPage,
  ],
  imports: [
    IonicPageModule.forChild(IntoleranciaReligiosaPage),
  ],
})
export class IntoleranciaReligiosaPageModule {}
