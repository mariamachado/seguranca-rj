import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiscriminacaoRacialPage } from './discriminacao-racial';

@NgModule({
  declarations: [
    DiscriminacaoRacialPage,
  ],
  imports: [
    IonicPageModule.forChild(DiscriminacaoRacialPage),
  ],
})
export class DiscriminacaoRacialPageModule {}
