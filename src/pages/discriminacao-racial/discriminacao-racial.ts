import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-discriminacao-racial',
  templateUrl: 'discriminacao-racial.html',
})
export class DiscriminacaoRacialPage{

  constructor(public navCtrl: NavController, public navParams: NavParams){}

  ionViewDidLoad(){}

}
