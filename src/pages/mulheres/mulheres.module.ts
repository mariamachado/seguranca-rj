import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MulheresPage } from './mulheres';

@NgModule({
  declarations: [
    MulheresPage,
  ],
  imports: [
    IonicPageModule.forChild(MulheresPage),
  ],
})
export class MulheresPageModule {}
