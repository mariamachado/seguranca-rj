import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-mulheres',
  templateUrl: 'mulheres.html',
})
export class MulheresPage{

  constructor(public navCtrl: NavController, public navParams: NavParams){}

  ionViewDidLoad(){}

}
