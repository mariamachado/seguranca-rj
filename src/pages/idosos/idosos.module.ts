import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IdososPage } from './idosos';

@NgModule({
  declarations: [
    IdososPage,
  ],
  imports: [
    IonicPageModule.forChild(IdososPage),
  ],
})
export class IdososPageModule {}
