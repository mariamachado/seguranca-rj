import { Component } from '@angular/core';
import { NavController, ActionSheetController, Platform } from 'ionic-angular';
import { MulheresPage } from "../mulheres/mulheres";
import { LgbtPage } from "../lgbt/lgbt";
import { DiscriminacaoRacialPage } from "../discriminacao-racial/discriminacao-racial";
import { IdososPage } from "../idosos/idosos";
import { CriancaAdolescentePage } from "../crianca-adolescente/crianca-adolescente";
import { IntoleranciaReligiosaPage } from "../intolerancia-religiosa/intolerancia-religiosa";
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage{

  constructor(
    public navCtrl: NavController, 
    public actionSheetCtrl: ActionSheetController, 
    public platform: Platform){}

  navMulheres(){
    this.navCtrl.push(MulheresPage);
  }

  navLgbt(){
    this.navCtrl.push(LgbtPage);
  }

  navDiscRacial(){
    this.navCtrl.push(DiscriminacaoRacialPage);
  }

  navIdosos(){
    this.navCtrl.push(IdososPage);
  }

  navCriancaAdol(){
    this.navCtrl.push(CriancaAdolescentePage);
  }

  navIntolRelig(){
    this.navCtrl.push(IntoleranciaReligiosaPage);
  }

  opcaoSair(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Tem certeza que deseja sair?',
      buttons: [
        {
          text: 'Sair',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'exit' : null,
          handler: () => {
            this.navCtrl.push(LoginPage); // Fazer logout
          }
        },{
          text: 'Cancelar',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {}
        }
      ]
    });
    actionSheet.present();
  }

}