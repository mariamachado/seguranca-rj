import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform } from 'ionic-angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { CallNumber } from '@ionic-native/call-number';
//import * as $ from 'jquery';

@IonicPage()
@Component({
  selector: 'page-enderecos',
  templateUrl: 'enderecos.html',
})
export class EnderecosPage {

  searchQuery: string = '';
  items: string[];
  destination: string;
  start: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    private _launchNavigator: LaunchNavigator,
    private _callNumber: CallNumber) {
    this.initializeItems();
  }

  initializeItems() {
    this.items = [
      'Amsterdam',
      'Bogota',
    ];
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  opcaoLigar() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Escolha um dos números',
      buttons: [
        {
          text: 'Plantão: 2334 5159',
          icon: !this.platform.is('ios') ? 'call' : null,
          handler: () => {
            this._callNumber.callNumber("2334 5159", true)
          }
        },
        {
          text: '5163 5142',
          icon: !this.platform.is('ios') ? 'call' : null,
          handler: () => {
            this._callNumber.callNumber("5163 5142", true)
          }
        },
        {
          text: '5163 5976',
          icon: !this.platform.is('ios') ? 'call' : null,
          handler: () => {
            this._callNumber.callNumber("5163 5976", true)
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => { }
        }
      ]
    });
    actionSheet.present();
  }

  tracaRota(address){
    this._launchNavigator.navigate(address);
  }

}