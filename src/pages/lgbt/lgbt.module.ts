import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LgbtPage } from './lgbt';

@NgModule({
  declarations: [
    LgbtPage,
  ],
  imports: [
    IonicPageModule.forChild(LgbtPage),
  ],
})
export class LgbtPageModule {}