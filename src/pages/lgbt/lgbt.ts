import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InformacoesPage } from './informacoes/informacoes';
import { EnderecosPage } from './enderecos/enderecos';

@IonicPage()
@Component({
  selector: 'page-lgbt',
  templateUrl: 'lgbt.html',
})
export class LgbtPage{

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams){}

  navInformacoes(){
    this.navCtrl.push(InformacoesPage);
  }

  navEnderecos(){
    this.navCtrl.push(EnderecosPage);
  }

}