import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DefinicoesPage } from './definicoes/definicoes';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

@IonicPage()
@Component({
  selector: 'page-informacoes',
  templateUrl: 'informacoes.html',
})
export class InformacoesPage{

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private _document: DocumentViewer){}

    navDefinicoes(){
      this.navCtrl.push(DefinicoesPage);
    }

    abrePDF(){
      const options: DocumentViewerOptions = {
        title: 'Arquivo PDF'
      }     
      this._document.viewDocument('assets/certificado-git.pdf', 'application/pdf', options)
    }    

}