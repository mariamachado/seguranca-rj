import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalhes-definicao',
  templateUrl: 'detalhes-definicao.html',
})
export class DetalhesDefinicaoPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {}

}
