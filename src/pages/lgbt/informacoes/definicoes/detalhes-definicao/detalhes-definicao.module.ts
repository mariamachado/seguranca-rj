import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesDefinicaoPage } from './detalhes-definicao';

@NgModule({
  declarations: [
    DetalhesDefinicaoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesDefinicaoPage),
  ],
})
export class DetalhesDefinicaoPageModule {}
