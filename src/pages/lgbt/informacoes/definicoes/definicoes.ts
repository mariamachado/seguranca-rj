import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetalhesDefinicaoPage } from './detalhes-definicao/detalhes-definicao';

@IonicPage()
@Component({
  selector: 'page-definicoes',
  templateUrl: 'definicoes.html',
})
export class DefinicoesPage {

  public items = ['Nome social', 'Nome social', 'Nome social'];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {}

    abreDescDefinicao(){
      this.navCtrl.push(DetalhesDefinicaoPage);
    }

}
