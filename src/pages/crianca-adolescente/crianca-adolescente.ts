import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-crianca-adolescente',
  templateUrl: 'crianca-adolescente.html',
})
export class CriancaAdolescentePage{

  constructor(public navCtrl: NavController, public navParams: NavParams){}

  ionViewDidLoad(){}

}
