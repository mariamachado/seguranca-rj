import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CriancaAdolescentePage } from './crianca-adolescente';

@NgModule({
  declarations: [
    CriancaAdolescentePage,
  ],
  imports: [
    IonicPageModule.forChild(CriancaAdolescentePage),
  ],
})
export class CriancaAdolescentePageModule {}
