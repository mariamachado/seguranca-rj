import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { Http } from '@angular/http';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    /* public http: Http*/) {} 

  efetuaLogin(){
    this.navCtrl.setRoot(HomePage);
  }

}