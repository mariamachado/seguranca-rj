webpackJsonp([11],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CriancaAdolescentePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CriancaAdolescentePage = (function () {
    function CriancaAdolescentePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CriancaAdolescentePage.prototype.ionViewDidLoad = function () { };
    CriancaAdolescentePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-crianca-adolescente',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/crianca-adolescente/crianca-adolescente.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Criança adolescente</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/crianca-adolescente/crianca-adolescente.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CriancaAdolescentePage);
    return CriancaAdolescentePage;
}());

//# sourceMappingURL=crianca-adolescente.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiscriminacaoRacialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DiscriminacaoRacialPage = (function () {
    function DiscriminacaoRacialPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DiscriminacaoRacialPage.prototype.ionViewDidLoad = function () { };
    DiscriminacaoRacialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-discriminacao-racial',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/discriminacao-racial/discriminacao-racial.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Discriminação racial</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/discriminacao-racial/discriminacao-racial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], DiscriminacaoRacialPage);
    return DiscriminacaoRacialPage;
}());

//# sourceMappingURL=discriminacao-racial.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IdososPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IdososPage = (function () {
    function IdososPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IdososPage.prototype.ionViewDidLoad = function () { };
    IdososPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-idosos',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/idosos/idosos.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Idosos</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/idosos/idosos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], IdososPage);
    return IdososPage;
}());

//# sourceMappingURL=idosos.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntoleranciaReligiosaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IntoleranciaReligiosaPage = (function () {
    function IntoleranciaReligiosaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IntoleranciaReligiosaPage.prototype.ionViewDidLoad = function () { };
    IntoleranciaReligiosaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-intolerancia-religiosa',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/intolerancia-religiosa/intolerancia-religiosa.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Intolerância religiosa</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/intolerancia-religiosa/intolerancia-religiosa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], IntoleranciaReligiosaPage);
    return IntoleranciaReligiosaPage;
}());

//# sourceMappingURL=intolerancia-religiosa.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnderecosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_launch_navigator__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__ = __webpack_require__(168);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import * as $ from 'jquery';
var EnderecosPage = (function () {
    function EnderecosPage(navCtrl, navParams, actionSheetCtrl, platform, _launchNavigator, _callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this._launchNavigator = _launchNavigator;
        this._callNumber = _callNumber;
        this.searchQuery = '';
        this.initializeItems();
    }
    EnderecosPage.prototype.initializeItems = function () {
        this.items = [
            'Amsterdam',
            'Bogota',
        ];
    };
    EnderecosPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    EnderecosPage.prototype.opcaoLigar = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Escolha um dos números',
            buttons: [
                {
                    text: 'Plantão: 2334 5159',
                    icon: !this.platform.is('ios') ? 'call' : null,
                    handler: function () {
                        _this._callNumber.callNumber("2334 5159", true);
                    }
                },
                {
                    text: '5163 5142',
                    icon: !this.platform.is('ios') ? 'call' : null,
                    handler: function () {
                        _this._callNumber.callNumber("5163 5142", true);
                    }
                },
                {
                    text: '5163 5976',
                    icon: !this.platform.is('ios') ? 'call' : null,
                    handler: function () {
                        _this._callNumber.callNumber("5163 5976", true);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    icon: !this.platform.is('ios') ? 'close' : null,
                    handler: function () { }
                }
            ]
        });
        actionSheet.present();
    };
    EnderecosPage.prototype.tracaRota = function (address) {
        this._launchNavigator.navigate(address);
    };
    EnderecosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-enderecos',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/enderecos/enderecos.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-searchbar (ionInput)="getItems($event)" placeholder="Pesquisar" showCancelButton cancelButtonText="Cancelar" animated="true"></ion-searchbar>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="border_content">\n  <div class="title_categoria">\n    <p>DGPC - Departamento Geral de Polícia da Capital</p>\n  </div>\n  <ion-list class="lista_enderecos">\n    <ion-item>\n      <div class="div_lista_enderecos">\n        <span>001ª DP – Praça Mauá</span>\n        <p>Praça Cristiano Ottoni, s/nº - Centro – Rio de Janeiro</p>\n        <p class="nome">Cláudio Vieira de Campos</p>\n        <p>cvcampos@pcivil.rj.gov.br</p>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-6>\n              <button ion-button block class="btn_enderecos" (click)="tracaRota(\'Mc donalds\')">traçar rota</button>\n            </ion-col>\n            <ion-col col-6>\n              <button ion-button block class="btn_enderecos" (click)="opcaoLigar()">ligar</button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n    </ion-item>\n    <!-- <ion-item *ngFor="let item of items">\n      {{ item }}\n    </ion-item> -->\n  </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/enderecos/enderecos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_call_number__["a" /* CallNumber */]])
    ], EnderecosPage);
    return EnderecosPage;
}());

//# sourceMappingURL=enderecos.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DefinicoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detalhes_definicao_detalhes_definicao__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DefinicoesPage = (function () {
    function DefinicoesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = ['Nome social', 'Nome social', 'Nome social'];
    }
    DefinicoesPage.prototype.abreDescDefinicao = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__detalhes_definicao_detalhes_definicao__["a" /* DetalhesDefinicaoPage */]);
    };
    DefinicoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-definicoes',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/informacoes/definicoes/definicoes.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>definições</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="border_content">\n  <div class="border_header"></div>\n  <ion-list class="lista_definicoes">\n    <button ion-item *ngFor="let item of items" (click)="abreDescDefinicao()">\n      {{ item }}\n    </button>  \n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/informacoes/definicoes/definicoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], DefinicoesPage);
    return DefinicoesPage;
}());

//# sourceMappingURL=definicoes.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalhesDefinicaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetalhesDefinicaoPage = (function () {
    function DetalhesDefinicaoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DetalhesDefinicaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalhes-definicao',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/informacoes/definicoes/detalhes-definicao/detalhes-definicao.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>definição</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="desc_detalhes_definicao">\n    <h1>Nome social</h1>\n    <p>Nome social é aquele assumido cotidianamente, no lugar do nome civil de batismo, de acordo com a aparência física.</p>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/informacoes/definicoes/detalhes-definicao/detalhes-definicao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], DetalhesDefinicaoPage);
    return DetalhesDefinicaoPage;
}());

//# sourceMappingURL=detalhes-definicao.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InformacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__definicoes_definicoes__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_document_viewer__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InformacoesPage = (function () {
    function InformacoesPage(navCtrl, navParams, _document) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._document = _document;
    }
    InformacoesPage.prototype.navDefinicoes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__definicoes_definicoes__["a" /* DefinicoesPage */]);
    };
    InformacoesPage.prototype.abrePDF = function () {
        var options = {
            title: 'Arquivo PDF'
        };
        this._document.viewDocument('assets/certificado-git.pdf', 'application/pdf', options);
    };
    InformacoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-informacoes',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/informacoes/informacoes.html"*/'<ion-header>\n    <ion-navbar class="bg_content_lgbt">\n        <ion-title>informações <span>LGBT</span></ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg_content_lgbt">\n    <div class="descricao">\n        <p>Identidade de Gênero refere-se à sensação interna de pertencer ao gênero feminino ou masculino. Ex.: Travestis, mulheres transexuais e homens trans.</p>\n        <p>A orientação sexual, diferentemente da identidade de gênero, é definida pela inclinação afetiva do indivíduo. Lésbicas, gays e bissexuais.</p>\n    </div>\n\n    <div class="btn_definicoes">\n        <a icon-right icon-end (click)="navDefinicoes()">\n            definições <ion-icon name="arrow-forward"></ion-icon>\n        </a>\n    </div>\n\n    <div class="arquivos">\n        <p>arquivos</p>\n\n        <ion-grid>\n            <ion-row>\n                <ion-col col-6> <!-- Loop lista de arquivos -->\n                    <ion-card class="card_arquivos">\n                        <button (click)="abrePDF()"> <!-- Testar PDF -->\n                            <ion-icon name="document"></ion-icon>\n                            <p>Projeto Violeta</p>\n                        </button>\n                    </ion-card>\n                </ion-col> \n\n                <ion-col col-6> <!-- Loop lista de arquivos -->\n                    <ion-card class="card_arquivos">\n                        <button (click)="abrePDF()">\n                            <ion-icon name="document"></ion-icon>\n                            <p>Projeto Violeta</p>\n                        </button>\n                    </ion-card>\n                </ion-col> \n            </ion-row>\n        </ion-grid>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/informacoes/informacoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_document_viewer__["a" /* DocumentViewer */]])
    ], InformacoesPage);
    return InformacoesPage;
}());

//# sourceMappingURL=informacoes.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MulheresPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MulheresPage = (function () {
    function MulheresPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MulheresPage.prototype.ionViewDidLoad = function () { };
    MulheresPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mulheres',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/mulheres/mulheres.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Mulheres</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/mulheres/mulheres.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], MulheresPage);
    return MulheresPage;
}());

//# sourceMappingURL=mulheres.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LgbtPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__informacoes_informacoes__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__enderecos_enderecos__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LgbtPage = (function () {
    function LgbtPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LgbtPage.prototype.navInformacoes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__informacoes_informacoes__["a" /* InformacoesPage */]);
    };
    LgbtPage.prototype.navEnderecos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__enderecos_enderecos__["a" /* EnderecosPage */]);
    };
    LgbtPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-lgbt',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/lgbt.html"*/'<ion-header>\n  <ion-navbar class="bg_content_lgbt"></ion-navbar>\n</ion-header>\n\n<ion-content class="bg_content_lgbt">\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n        <ion-title>LGBT</ion-title>\n      </ion-col>\n      <ion-col col-6>\n        <div class="img_guias">\n            <img src="#">\n        </div>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col col-6>\n        <a href="#" (click)="navInformacoes()" class="card_links">\n          <ion-card>\n            <ion-icon name="information-circle"></ion-icon>\n            <p>informações</p>\n          </ion-card>\n        </a>\n      </ion-col>\n      <ion-col col-6>\n        <a href="#" (click)="navEnderecos()" class="card_links">\n          <ion-card>\n            <ion-icon name="pin"></ion-icon>\n            <p>endereços</p>\n          </ion-card>\n        </a>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <div class="passo_passo">\n    <p>passo a passo</p>\n\n    <ion-card class="card_passo_passo"> <!-- Loop lista passo a passo -->\n      <span>1</span>\n      <p>Preocupe-se com o socorro à vítima e a <a href="#">preservação do local do crime</a></p> <!-- Pode ter links para detalhes-definicao ou para um arquivo pdf -->\n    </ion-card>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/lgbt/lgbt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], LgbtPage);
    return LgbtPage;
}());

//# sourceMappingURL=lgbt.js.map

/***/ }),

/***/ 123:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 123;

/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/crianca-adolescente/crianca-adolescente.module": [
		293,
		10
	],
	"../pages/discriminacao-racial/discriminacao-racial.module": [
		294,
		9
	],
	"../pages/idosos/idosos.module": [
		295,
		8
	],
	"../pages/intolerancia-religiosa/intolerancia-religiosa.module": [
		296,
		7
	],
	"../pages/lgbt/enderecos/enderecos.module": [
		297,
		6
	],
	"../pages/lgbt/informacoes/definicoes/definicoes.module": [
		298,
		5
	],
	"../pages/lgbt/informacoes/definicoes/detalhes-definicao/detalhes-definicao.module": [
		300,
		4
	],
	"../pages/lgbt/informacoes/informacoes.module": [
		299,
		3
	],
	"../pages/lgbt/lgbt.module": [
		302,
		2
	],
	"../pages/login/login.module": [
		301,
		1
	],
	"../pages/mulheres/mulheres.module": [
		303,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 165;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mulheres_mulheres__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__lgbt_lgbt__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__discriminacao_racial_discriminacao_racial__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__idosos_idosos__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__crianca_adolescente_crianca_adolescente__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__intolerancia_religiosa_intolerancia_religiosa__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = (function () {
    function HomePage(navCtrl, actionSheetCtrl, platform) {
        this.navCtrl = navCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
    }
    HomePage.prototype.navMulheres = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__mulheres_mulheres__["a" /* MulheresPage */]);
    };
    HomePage.prototype.navLgbt = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__lgbt_lgbt__["a" /* LgbtPage */]);
    };
    HomePage.prototype.navDiscRacial = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__discriminacao_racial_discriminacao_racial__["a" /* DiscriminacaoRacialPage */]);
    };
    HomePage.prototype.navIdosos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__idosos_idosos__["a" /* IdososPage */]);
    };
    HomePage.prototype.navCriancaAdol = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__crianca_adolescente_crianca_adolescente__["a" /* CriancaAdolescentePage */]);
    };
    HomePage.prototype.navIntolRelig = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__intolerancia_religiosa_intolerancia_religiosa__["a" /* IntoleranciaReligiosaPage */]);
    };
    HomePage.prototype.opcaoSair = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Tem certeza que deseja sair?',
            buttons: [
                {
                    text: 'Sair',
                    role: 'destructive',
                    icon: !this.platform.is('ios') ? 'exit' : null,
                    handler: function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */]); // Fazer logout
                    }
                }, {
                    text: 'Cancelar',
                    role: 'cancel',
                    icon: !this.platform.is('ios') ? 'close' : null,
                    handler: function () { }
                }
            ]
        });
        actionSheet.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title text-center>\n      Guias\n    </ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="opcaoSair()">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons>    \n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <p class="p_home">Escolha um dos grupos para ver as informações</p>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n        <button class="mulheres" (click)="navMulheres()">\n          <div class="img_guias">\n            <img src="#">\n          </div>\n          <span>Mulheres</span>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button class="lgbt" (click)="navLgbt()">\n          <div class="img_guias">\n            <img src="#">\n          </div>\n          <span>LGBT</span>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button class="discriminacao" (click)="navDiscRacial()">\n          <div class="img_guias">\n            <img src="#">\n          </div>\n          <span>Discriminação racial</span>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button class="idosos" (click)="navIdosos()">\n          <div class="img_guias">\n            <img src="#">\n          </div>\n          <span>Idosos</span>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button class="crianca" (click)="navCriancaAdol()">\n          <div class="img_guias">\n            <img src="#">\n          </div>\n          <span>Criança e adolescente</span>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button class="intolerancia" (click)="navIntolRelig()">\n          <div class="img_guias">\n            <img src="#">\n          </div>\n          <span>Intolerância religiosa</span>\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(234);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_mulheres_mulheres__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_lgbt_lgbt__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_discriminacao_racial_discriminacao_racial__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_idosos_idosos__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_crianca_adolescente_crianca_adolescente__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_intolerancia_religiosa_intolerancia_religiosa__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_lgbt_informacoes_informacoes__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_lgbt_informacoes_definicoes_definicoes__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_lgbt_informacoes_definicoes_detalhes_definicao_detalhes_definicao__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_lgbt_enderecos_enderecos__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_usuarios_usuarios__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_document_viewer__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_launch_navigator__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_call_number__ = __webpack_require__(168);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




















// Recursos nativos adicionais



var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_mulheres_mulheres__["a" /* MulheresPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_lgbt_lgbt__["a" /* LgbtPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_discriminacao_racial_discriminacao_racial__["a" /* DiscriminacaoRacialPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_idosos_idosos__["a" /* IdososPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_crianca_adolescente_crianca_adolescente__["a" /* CriancaAdolescentePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_intolerancia_religiosa_intolerancia_religiosa__["a" /* IntoleranciaReligiosaPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_lgbt_informacoes_informacoes__["a" /* InformacoesPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_lgbt_informacoes_definicoes_definicoes__["a" /* DefinicoesPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_lgbt_informacoes_definicoes_detalhes_definicao_detalhes_definicao__["a" /* DetalhesDefinicaoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_lgbt_enderecos_enderecos__["a" /* EnderecosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/crianca-adolescente/crianca-adolescente.module#CriancaAdolescentePageModule', name: 'CriancaAdolescentePage', segment: 'crianca-adolescente', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/discriminacao-racial/discriminacao-racial.module#DiscriminacaoRacialPageModule', name: 'DiscriminacaoRacialPage', segment: 'discriminacao-racial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/idosos/idosos.module#IdososPageModule', name: 'IdososPage', segment: 'idosos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/intolerancia-religiosa/intolerancia-religiosa.module#IntoleranciaReligiosaPageModule', name: 'IntoleranciaReligiosaPage', segment: 'intolerancia-religiosa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lgbt/enderecos/enderecos.module#EnderecosPageModule', name: 'EnderecosPage', segment: 'enderecos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lgbt/informacoes/definicoes/definicoes.module#DefinicoesPageModule', name: 'DefinicoesPage', segment: 'definicoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lgbt/informacoes/informacoes.module#InformacoesPageModule', name: 'InformacoesPage', segment: 'informacoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lgbt/informacoes/definicoes/detalhes-definicao/detalhes-definicao.module#DetalhesDefinicaoPageModule', name: 'DetalhesDefinicaoPage', segment: 'detalhes-definicao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/lgbt/lgbt.module#LgbtPageModule', name: 'LgbtPage', segment: 'lgbt', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mulheres/mulheres.module#MulheresPageModule', name: 'MulheresPage', segment: 'mulheres', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* HttpModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_mulheres_mulheres__["a" /* MulheresPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_lgbt_lgbt__["a" /* LgbtPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_discriminacao_racial_discriminacao_racial__["a" /* DiscriminacaoRacialPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_idosos_idosos__["a" /* IdososPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_crianca_adolescente_crianca_adolescente__["a" /* CriancaAdolescentePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_intolerancia_religiosa_intolerancia_religiosa__["a" /* IntoleranciaReligiosaPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_lgbt_informacoes_informacoes__["a" /* InformacoesPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_lgbt_informacoes_definicoes_definicoes__["a" /* DefinicoesPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_lgbt_informacoes_definicoes_detalhes_definicao_detalhes_definicao__["a" /* DetalhesDefinicaoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_lgbt_enderecos_enderecos__["a" /* EnderecosPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_usuarios_usuarios__["a" /* UsuariosProvider */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_document_viewer__["a" /* DocumentViewer */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_call_number__["a" /* CallNumber */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsuariosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UsuariosProvider = (function () {
    function UsuariosProvider(http) {
        this.http = http;
    }
    UsuariosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], UsuariosProvider);
    return UsuariosProvider;
}());

//# sourceMappingURL=usuarios.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { Http } from '@angular/http';

var LoginPage = (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.efetuaLogin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/login/login.html"*/'<ion-content class="bg_login">\n  <div class="img_login"></div>\n\n  <form class="form_login" (submit)="efetuaLogin()">\n    <ion-input type="text" name="nome" placeholder="e-mail"></ion-input>\n    <ion-input type="email" name="email" placeholder="senha"></ion-input>\n    <a href="#">esqueci minha senha</a>\n    <button ion-button type="submit">entrar</button>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/mariavictoria/Desktop/workspace/seguranca-rj/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[212]);
//# sourceMappingURL=main.js.map